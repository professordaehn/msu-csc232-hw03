/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Customer.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Implementation of Customer class.
 */

#include "Customer.h"

Customer::Customer(const std::string &customerName, const Duration &duration,
                   const ClockTime &arrived) : name{customerName},
                                               transactionDuration{duration},
                                               arrivalTime{arrived} {
    /* no-op */
}

std::string Customer::getName() const {
    return name;
}

Duration Customer::getTransactionDuration() const {
    return transactionDuration;
}

void Customer::setDepartureTime(const ClockTime &time) {
    departureTime = time;
}

Duration Customer::getWaitTime() const {
    return departureTime - arrivalTime;
}