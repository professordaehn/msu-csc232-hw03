/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Customer.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Customer class specification.
 */

#ifndef BANKSIMULATION_CUSTOMER_H
#define BANKSIMULATION_CUSTOMER_H

#include <string>

/**
 * Custom type to represent a duration of time. This is nothing but an alias for an unsigned int.
 */
using Duration = size_t;

/**
 * Custom type to represent a moment in time. This is nothing but an alias for an unsigned int.
 */
using ClockTime = size_t;

/**
 * A customer in the bank simulation.
 */
class Customer {
public:
    /**
     * Customer initializing constructor.
     *
     * @param customerName the name of this Customer
     * @param duration the duration required for the transaction associated with this customer
     * @param arrived the time this Customer arrived at the bank
     */
    Customer(const std::string& customerName, const Duration& duration, const ClockTime& arrived);

    /**
     * Name accessor for this Customer.
     *
     * @return The name of this Customer is returned.
     */
    std::string getName() const;

    /**
     * Transaction duration access for this Customer.
     *
     * @return The time to process the transaction for this Customer.
     */
    Duration getTransactionDuration() const;

    /**
     * Set the time this Customer departs from the bank.
     *
     * @param time The time this Customer departs from the bank is returned.
     */
    void setDepartureTime(const ClockTime& time);

    /**
     * Get the time this Customer took to be processed.
     *
     * @return The time it took this Customer to be processed is returned.
     */
    Duration getWaitTime() const;
private:
    std::string name;
    Duration transactionDuration;
    ClockTime arrivalTime;
    ClockTime departureTime;
};

#endif //BANKSIMULATION_CUSTOMER_H
