/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Main.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Contains main(), the entry point of this homework assignment.
 */

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <queue>
#include <sstream>

#include "Event.h"
#include "Customer.h"

/**
 * Forward declaration of the processArrival function that processes arrival events.
 *
 * @param event an arrival event to be processed
 * @param eventQueue the (priority) queue of events in the bank simulation
 * @param bankLine the queue of customers in the bank simulation
 */
void processArrival(Event event, std::priority_queue<Event>& eventQueue, std::queue<Customer>& bankLine);

/**
 * Forward declaration of the processDeparture function that processes departure events.
 *
 * @param event a departure event to be processed
 * @param eventQueue the (priority) queue of events in the bank simulation
 * @param bankLine the queue of customers in the bank simulation
 */
void processDeparture(Event event, std::priority_queue<Event>& eventQueue, std::queue<Customer>& bankLine);

// Global variables
static const int FIRST_COLUMN_WIDTH{38};
static const int SECOND_COLUMN_WIDTH{2};
static bool tellerAvailable{true};
static ClockTime currentTime{0};
static ClockTime cumulativeTime{0};

/**
 * Entry point of this homework assignment.
 *
 * @return EXIT_SUCCESS is returned upon completion of this function.
 */
int main() {
    std::cout << "Simulation Begins" << std::endl;
    size_t numCustomers{0};
    double averageTime{0};

    std::ifstream inFile("data.txt");

    if (!inFile) {
        std::cerr << "Problem opening data.txt!\nPlease make sure data.txt exists in the binary source folder.";
        exit(EXIT_FAILURE);
    }

    std::queue<Customer> bankLine;
    std::priority_queue<Event> eventQueue;

    // Create and add arrival events to event queue
    while(inFile.peek() != EOF) {
        numCustomers++;
        ClockTime arrivalTime;
        Duration transactionDuration;

        inFile >> arrivalTime;
        inFile >> transactionDuration;

        Event event = Event{arrivalTime, transactionDuration};
        eventQueue.push(event);
    }

    // Event loop
    while (!eventQueue.empty()) {
        Event event = eventQueue.top();

        // Get current time
        currentTime = event.time();
        if (event.type() == 'A') {
            processArrival(event, eventQueue, bankLine);
        } else {
            processDeparture(event, eventQueue, bankLine);
        }
    }

    inFile.close();
    averageTime = (static_cast<double>(cumulativeTime)) / numCustomers;

    std::cout << "Simulation Ends" << std::endl << std::endl;
    std::cout << "Final Statistics:" << std::endl;
    std::cout << "\tTotal number of people processed: " << numCustomers << std::endl;
    std::cout << "\tAverage amount of time spent waiting: " << averageTime << std::endl;

    return EXIT_SUCCESS;
}

void processArrival(Event event, std::priority_queue<Event>& eventQueue, std::queue<Customer>& bankLine) {
    // TODO: Implement me (see pages 407-408 for help)
}

void processDeparture(Event event, std::priority_queue<Event>& eventQueue, std::queue<Customer>& bankLine) {
  // TODO: Implement me (see pages 407-408 for help)
}
