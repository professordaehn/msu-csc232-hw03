/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Demo.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Entry point for demo application, which simply shows how to open and close a file.
 */

#include <cstdlib>
#include <iostream>
#include <fstream>

int main(int argc, char *argv[]) {
    std::cout << "Bank Simulation (Demo)" << std::endl << std::endl;

    std::ifstream inFile("data.txt");

    if (!inFile) {
        // You should not see this message if the file opened properly.
        std::cerr << "Problem opening data.txt!";
    }

    inFile.close();

    return EXIT_SUCCESS;
}