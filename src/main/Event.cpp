/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Event.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Event class implementation.
 */

#include "Event.h"

Event::Event(const ClockTime &arrivalTime, const Duration &duration, const EventType &type) : eventType{type},
                                                                                              eventTime{arrivalTime},
                                                                                              eventDuration{duration} {
    // no-op
}

Event::~Event() {
    // no-op
}

EventType Event::type() const {
    return eventType;
}

ClockTime Event::time() const {
    return eventTime;
}

Duration Event::duration() const {
    return eventDuration;
}

bool operator<(const Event &lhs, const Event &rhs) {
    // when prioritizing by time, a larger number has a lower priority
    return lhs.time() > rhs.time();
}

std::ostream &operator<<(std::ostream &os, const Event &event) {
    os << "Event type: " << event.type() << ", Event time: " << event.time() << ", Event duration: "
       << event.duration();
    return os;
}