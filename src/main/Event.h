/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    Event.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 *          Your Name <yname@missouristate.edu>
 *          Partner Name <pname@missouristate.edu>
 * @brief   Event class specification.
 */

#ifndef BANKSIMULATION_ARRIVALEVENT_H
#define BANKSIMULATION_ARRIVALEVENT_H


#include <cstdlib>
#include <iostream>

/**
 * Custom type to represent a duration of time. This is nothing but an alias for an unsigned int.
 */
using ClockTime = size_t;

/**
 * Custom type to represent a moment in time. This is nothing but an alias for an unsigned int.
 */
using Duration = size_t;

/**
 * Customer type to represent different event types. This is nothing more than an alias for a char.
 */
using EventType = char;

/**
 * An abstraction of an Event that occurs while processing bank transactions in a bank simulation.
 */
class Event {
public:
    /**
     * Initializing constructor.
     *
     * @param arrivalTime the time this event commenced
     * @param duration the length of time it takes to complete this event
     * @param type 'A' for arrival event; 'D' for departure event
     */
    Event(const ClockTime &arrivalTime, const Duration &duration, const EventType& type = 'A');

    /**
     * Class destructor.
     */
    ~Event();

    /**
     * Get the type of event.
     *
     * @return The character 'A' is returned if this is an arrival event, otherwise the character 'D' is returned to
     * signify a departure event.
     */
    EventType type() const;

    /**
     * Get the time this event commenced.
     *
     * @return The time this event commenced is returned.
     */
    ClockTime time() const;

    /**
     * Get the duration of this event.
     *
     * @return The duration of this event is returned.
     */
    Duration duration() const;

    /**
     * Provide the convenience of inserting event objects into an ostream.
     *
     * @param os the ostream this event is inserted
     * @param event this event
     * @return A reference to an ostream that allows for chaining of ostream insertion is returned.
     */
    friend std::ostream &operator<<(std::ostream &os, const Event &event);

    /**
     * Provide a way for the priority queue to compare events for priority.
     *
     * @param lhs the left-hand side argument, i.e., a, in a < b
     * @param rhs the right-hand side argument, i.e., b in a < b
     *
     * @return True is returned in the left-hand side event is earlier in time than the right-hand side event;
     * false otherwise
     */
    friend bool operator<(const Event& lhs, const Event& rhs);
private:
    EventType eventType;
    ClockTime eventTime;
    Duration eventDuration;
};

#endif //BANKSIMULATION_ARRIVALEVENT_H
