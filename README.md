# Homework 3 - Bank Simulation

This homework is essentially to complete the work here to implement the solution to Programming Problem 6 in Chapter 13 - Queues and Priority Queues. When completed successfully, the output of the Main program should match that found on the top of page 414 with the following exceptions:

* The "first column" of the simulation is left-justified in a field with of 38 characters
* the "second column" of the simulation output is right-justified in a field width of 2 characters.

## Getting Started

As usual:

* Fork this repo into your own, private, repo.
* Clone your private repo
* Checkout the develop branch before you begin your coding
* Commit, minimally, after implementing the processArrival() and processDeparture() functions.
* After your final commit, create a pull request that seeks to merge _your_ develop branch into _your_ master branch
* Submit the URL of this pull request as a text submission for the associated assignment on Blackboard

## Goal

Follow the directions in `src/main/Main.cpp`.

## Generating Projects

As usual, this repo contains a number of `cmake` generator options. For example, navigate to the `build/unix` folder and run `cmake` to generate Unix make files that can be used to easily create executables from the source code.

```bash
$ cd build/unix
$ cmake -G "Unix Makefiles" ../..
... lots of output ...
$ make
... lots of output ...
$ ./BankSimulation
Simulation Begins
Processing an arrival event at time:   1
Processing an arrival event at time:   2
Processing an arrival event at time:   4
Processing a departure event at time:  6
Processing a departure event at time: 11
Processing a departure event at time: 16
Processing an arrival event at time:  20
Processing an arrival event at time:  22
Processing an arrival event at time:  24
Processing a departure event at time: 25
Processing an arrival event at time:  26
Processing an arrival event at time:  28
Processing a departure event at time: 30
Processing an arrival event at time:  30
Processing a departure event at time: 35
Processing a departure event at time: 40
Processing a departure event at time: 45
Processing a departure event at time: 50
Processing an arrival event at time:  88
Processing a departure event at time: 91
Simulation Ends

Final Statistics:
        Total number of people processed: 10
        Average amount of time spent waiting: 5.6
$ ./BankSimulationDemo
Bank Simulation (Demo)

$ ./BankSimulationTest
OK (0)
$
```

CAUTION - When you first checkout this project, the simulation will run as an endless loop. This is corrected by a correct implementation of `processArrival()` and `processDeparture()`

NOTE: This assignment utilizes the STL version of queue and priority queue. As such, the API is a bit different. See this documentation on [queues](http://en.cppreference.com/w/cpp/container/queue) and [priority queues](http://en.cppreference.com/w/cpp/container/priority_queue) for more detail.

PS - Your instructor will only support questions related to the unix builds; if you choose to use a different build environment, it is your responsibility to determine where to place the data.txt so that it is in a location readable by your executable.
