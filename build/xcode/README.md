# Xcode
This directory should be your target for Xcode project files. To generate Xcode project files, type

```
$ cmake -G "Xcode" ../..
```

at the command line prompt.
