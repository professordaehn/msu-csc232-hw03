cmake_minimum_required(VERSION 3.7)
project(BankSimulation)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g")
if(MSVC)
    # Force to always compile with W4
    if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
        string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
    endif()
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    # Update if necessary
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-long-long -pedantic")
endif()

find_library(CPPUNIT_LIBRARY_DEBUG
        NAMES cppunit
        cppunit_dll
        cppunitd
        cppunitd_dll
        PATHS /usr/lib
        /usr/lib64
        /usr/local/lib
        /usr/local/lib64
        PATH_SUFFIXES debug)
find_library(CPPUNIT_LIBRARY_RELEASE
        NAMES cppunit
        cppunit_dll
        cppunitd
        cppunitd_dll
        PATHS /usr/lib
        /usr/lib64
        /usr/local
        /usr/local/lib64
        PATH_SUFFIXES release)

if (CPPUNIT_LIBRARY_DEBUG AND NOT CPPUNIT_LIBRARY_RELEASE)
    set(CPPUNIT_LIBRARY ${CPPUNIT_DEBUG})
endif (CPPUNIT_LIBRARY_DEBUG AND NOT CPPUNIT_LIBRARY_RELEASE)

set(CPPUNIT_LIBRARY
        debug ${CPPUNIT_LIBRARY_DEBUG}
        optimized ${CPPUNIT_LIBRARY_RELEASE})

set(CMAKE_CXX_STANDARD 14)
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/data.txt
        DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

set(SRC_MAIN_FILES src/main/Main.cpp src/main/Event.h src/main/Event.cpp src/main/Customer.cpp src/main/Customer.h)
set(SRC_DEMO_FILES src/main/Demo.cpp src/main/Event.h src/main/Event.cpp src/main/Customer.cpp src/main/Customer.h)
set(SRC_TEST_FILES src/test/UnitTestRunner.cpp src/main/Event.h src/main/Event.cpp src/main/Customer.cpp src/main/Customer.h)

add_executable(BankSimulation ${SRC_MAIN_FILES})
add_executable(BankSimulationDemo ${SRC_DEMO_FILES})
add_executable(BankSimulationTest ${SRC_TEST_FILES})

if (APPLE)
    include_directories(BankSimulationTest PUBLIC /usr/local/include)
endif (APPLE)

target_link_libraries(BankSimulationTest PUBLIC ${CPPUNIT_LIBRARY})
